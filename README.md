# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento da Programação Funcional](capitulos/surgimento_da_programacao_funcional.md)
2. [Primeiros Computadores Pré Modernos](capitulos/primeiros_computadores_pre_modernos.md)
3. [Primeira Geração (1946-1954)](capitulos/primeira_geracao.md)
4. [Segunda Geração (1955-1964)](capitulos/segunda_geracao.md)
5. [Terceira Geração (1964-1975)](capitulos/terceira_geracao.md)
6. [Quarta Geração (1975-Atualidade)](capitulos/quarta_geracao.md) 
7. [Computadores Quânticos](capitulos/computadores_quanticos.md)



## Autores
Esse livro foi escrito por:

Avatar|Nome | Nickname| E-mail 
---|---|---|---
<img src="imagens/avatar_anne.png" width="100">|Anne Caroline Reolon Dalla Costa | @anne_caroliner | [annecarolinereolon@gmail.com](mailto:annecarolinereolon@gmail.com)
<img src="imagens/avatar_cinthia.png" width="100">|Cinthia Martins | @Cinthia-Martins92 | [cinthia.amarttins@gmail.com](mailto:cinthia.amarttins@gmail.com)
<img src="imagens/avatar_edgar.jpeg" width="100">|Edgar Weippert | @nicoweippert15 | [nicoweippert15@gmail.com](mailto:edgarweippert@gmail.com)
<img src="imagens/avatar_pedro.png" width="100">|Pedro Quinellato Dutra Vieira | @pedroqdvieira | [pedroqdvieira88123@gmail.com](mailto:pedroqdvieira88123@gmail.com)
