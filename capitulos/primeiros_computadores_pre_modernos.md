**A Máquina de Diferenças e o Engenho Analítico**
Em 1822 foi publicado um artigo científico que prometia revolucionar tudo o que existia até então no ramo do cálculo eletrônico. O autor, Charles Babbage, afirmou que a máquina era capaz de calcular funções de diversas naturezas (trigonometria, logaritmos) de forma muito simples. Esse projeto foi chamado de Máquina de Diferenças.

<img src="imagens/img_Difference_engine_Scheutz.jpg" width="410" height="314">

Após um período, em 1837, Babbage lançou uma nova máquina, chamada de Engenho Analítico (Máquina Analítica), que aproveitava todos os conceitos do Tear Programável, como o uso dos cartões. Além disso, instruções e comandos podiam ser informados pelos cartões, fazendo uso de registradores primitivos. A precisão chegava a 50 casas decimais. Novamente, ela não pôde ser implementada na época por conta de limitações técnicas e financeiras. A tecnologia existente não era avançada o suficiente para a execução do projeto; contudo, a contribuição teórica de Babbage foi tão grande que muitas de suas ideias são usadas até hoje.

**A Teoria de Boole**

Se Babbage é o avô do computador do ponto de vista de arquitetura de hardware, o matemático George Boole pode ser considerado o pai da lógica moderna. Ele desenvolveu, em 1847, um sistema lógico que reduzia a representação de valores com dois algarismos: 0 e 1. Em sua teoria, o número 1 tem significados como: ativo, ligado, existente, verdadeiro; e 0 representa o inverso: não ativo, desligado, não existente, falso. Para indicar valores intermediários, como "mais ou menos" ativo, é possível usar dois ou mais algarismos (bits) para a representação. Por exemplo:

00: desligado
01: carga baixa
10: carga moderada
11: carga alta
Todo o sistema lógico dos computadores atuais usa a Teoria de Boole de forma prática.




**Computadores pré-modernos**

Na primeira metade do século XX, vários computadores mecânicos foram desenvolvidos, e com o passar do tempo componentes eletrônicos foram adicionados aos projetos. Em 1931, Vannevar Bush implementou em um computador uma arquitetura binária propriamente dita usando os bits 0 e 1. A base decimal exigia que a eletricidade assumisse 10 voltagens, o que era muito difícil de ser controlado, por isso Bush usou a lógica de Boole, em que somente 2 níveis de voltagem eram suficientes.

A Segunda Guerra Mundial foi um grande incentivo no desenvolvimento de computadores, visto que as máquinas estavam se tornando mais úteis em tarefas de desencriptação de mensagens inimigas e criação de armas mais inteligentes. Entre os projetos desenvolvidos no período, os que mais se destacaram foram o Mark I, em 1944, desenvolvido na Universidade Harvard (EUA), e o Colossus, em 1946, criado por Allan Turing.

<img src="imagens/img_Harvard_Mark_I_Computer_-_Left_Segment.jpg" width="388" height="314">


Sendo uma das figuras mais importantes da computação, Turing focou sua pesquisa na descoberta de problemas formais e práticos que poderiam ser resolvidos por computadores. Para aqueles que apresentavam solução, foi criada a famosa teoria da Máquina de Turing, que, com um número finito de operações, resolvia problemas computacionais de diversas ordens. A ideia foi colocada em prática com o Colossus.



Referências: <https://www.tecmundo.com.br/tecnologia-da-informacao/1697-a-historia-dos-computadores-e-da-computacao.htm>

