**Paradigma de Programação**


Linguagens de Programação são naturalmente criadas seguindo um modelo de funcionamento, uma forma de execução e um ambiente onde são executadas, os Paradigmas de Programação compreendem a forma como uma linguagem de programação trabalha, é o estilo de programar, linguagens mais antigas eram comumente imperativas e lineares, de forma que o programa era escrito na sequência em que deveria ser executado, e descrevendo passo a passo como uma rotina deveria funcionar. Com a evolução da tecnologia porém, novos conceitos ou paradigmas foram pensados para melhorar a escrita, o reaproveitamento de código tanto na produção quanto na manutenção de códigos em programas, e então surgem a Programação Orientada a Objetos, além de outros paradigmas, a ideia é sempre de reutilização de código, fácil manutenção, e abstração dos problemas, criando rotinas cada vez mais inteligentes e abstratas, sendo reutilizadas em inúmeras situações.


A Programação Funcional é mais um paradigma de programação, logo é uma forma ou um estilo de programar. Algumas linguagens como Lisp, Haskell, Erlang e Elixir são totalmente criadas utilizando este paradigma, de modo que programadores nestas linguagens precisam conhecer o conceito para tirar maior proveito dos recursos da PF Abreviação para Programação Funcional, embora pareça Polícia Federal mas não, neste texto e nos próximos PF refere-se a Programação Funcional.


Recentemente com o advento de bibliotecas populares como o React que trabalham sobre o paradigma da programação funcional, torna-se necessário explorarmos os conceitos de como a Programação Funcional trabalha, quais são os recursos desse paradigma e por que utilizar a Programação Funcional no desenvolvimento de aplicações web.

Uma das grandes vantagens da programação funcional, é utilizar seus recursos em aplicações de larga escala, assim, recursos como imutabilidade, funções de primeira ordem e tantos outros, evitam ou até minimizam efeitos colaterais em aplicações de larga escala.

Bibliotecas como React, utilizam este paradigma de programação e conseguem obter performance, organização e escalabilidade. Nos próximos posts a medida que entendemos os recursos da programação funcional, veremos também como aplicá-la na prática com bibliotecas poderosas como RambdaJS e Lodash.

**Programação Funcional a Origem**



A origem da PF está no mundo acadêmico em especial na matemática, em tratar a programação como funções matemáticas, e seguindo princípios do Cálculo Lambda, cálculo-λ um conceito que trata de recursividade e onde as entidades podem ser passadas como parâmetros e retornada como valores dentro de outras funções além de tratar de da evolução dos dados, neste conceitos os dados não são alterados, eles evoluem, esse conceito serve de base para a criação de várias linguagens de programação.

Análise Matemática, estatística, análise financeira, aplicações concorrentes estão entre os objetivos do uso da PF. Algumas linguagens são puramente funcionais como: Lisp, Haskel, Elixir já outras são multiparadigmas, ou seja, permitem mais de um paradigma: Erlang, R, Scala. O JavaScript também é uma linguagem multiparadigma, e é possível utilizar o JavaScript de forma funcional. 

Embora somente o uso de funções na programação não significa que está necessariamente utilizando esse paradigma, essa é uma das principais características. Benefícios da Programação Funcional


**> Por que utilizar a programação funcional?** 



**Evita efeitos colaterais**
Uma vez que os dados são controlados por funções puras e dados imutáveis, podemos escalar a aplicação, sem a preocupação de side effects. Um componente irá se comportar conforme ele foi programado.

**É limpo e não verboso**

Uma vez que a programação declarativa e imutável está presente, temos uma redução na escrita do código, códigos mais limpos e objetivos tendem a ter maior facilidade de manutenibilidade.

**Trabalho em equipe**

É excelente para trabalho em equipe, uma vez que os códigos que foram desenvolvidos nesse paradigma sempre se comportam exatamente como foram concebidos originalmente. 

**A programação funcional**


Em todas as máquinas e mecanismos mostrados, as operações estavam previamente programadas, não sendo possível inserir novas funções. Contudo, em 1801, o costureiro Joseph Marie Jacquard desenvolveu um sistema muito interessante. A indústria de Jacquard atuava no ramo de desenhos em tecidos, tarefa que ocupava muito tempo de trabalho manual. Vendo esse problema, ele construiu a primeira máquina realmente programável, com o objetivo de recortar os tecidos de forma automática.

Tal mecanismo foi chamado de Tear Programável, pois aceitava cartões com entrada do sistema. Dessa maneira, Jacquard perfurava o cartão com o desenho desejado e a máquina o reproduzia no tecido. A partir daquele momento, muitos esquemas foram influenciados.
