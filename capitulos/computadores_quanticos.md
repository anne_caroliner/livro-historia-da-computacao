# SUPER COMPUTADORES


<img src="imagens/img_um.jpeg" width="410" height="314">


O "pai" da computação quântica é o físico Paul Benioff que propôs, na década de 80, a criação de máquinas que usassem princípios da física quântica para funcionar. Esse uso foi pensado para ir além das capacidades dos computadores convencionais. Com esta ideia em mente, cientistas começaram a desenvolver os primeiros protótipos e, a partir deles, traçaram os primeiros experimentos com a nova tecnologia, em 1990.


## O QUE É UM COMPUTADOR QUÂNTICO

Um computador quântico é um dispositivo que executa cálculos fazendo uso direto de propriedades da mecânica quântica, tais como sobreposição e interferência. Enquanto um computador eletrônico convencional manipula informações a partir de valores que são representados por números binários (0 ou 1), um computador quântico usa qubits, os bits quânticos, que podem assumir valor 0, 1 ou as duas coisas ao mesmo tempo.

## APLICAÇÕES

computadores quânticos têm, hoje, um escopo limitado de aplicações. Os principais tipos de uso estão relacionados com pesquisas científicas: estudos nas áreas de astrofísica, física e matemática são alguns exemplos. Mas aplicações com resultados a curto prazo também são possíveis, como simulações extremamente precisas e complexas do comportamento do clima no planeta. Outros estudos discutidos são da área de biotecnologia e de medicina de ponta, que conseguiriam transformar a tecnologia existente nos computadores para reproduzir estudos avançados sobre o comportamento de proteínas, órgãos e células do corpo humano.


<img src="imagens/img_dois.jpeg" width="410" height="314">



