# SEGUNDA GERAÇÃO DE COMPUTADORES


A segunda geração de computadores vem com o marco dos transistores, que vieram para substituir as válvulas eletrônicas. Esta geração traz uma novidade que além de ser consumir menos energia e ter um tamanho mais compacto, também era mais barato.
Nesta geração a linguagens de programação de alto nível, como, FORTRAN e COBOL passam a ser utilizadas.

# IBM 7030


<img src="imagens/img_ibm7030.jpeg" width="388" height="314">


Conhecido como o computador da classe STRETCH, o IBM 7030 é o mais rápido, poderoso e versátil do mundo, foi o primeiro computador transistorizado da IBM e se tornou operacional no ano de 1964.

O sistema IBM 7030 possui transistores extremamente rápidos e componentes de circuitos, porém o seu princípio de operação simultânea é o que faz com que a máquina tenha uma velocidade realmente muito grande. O sistema agora possui um novo modelo de armazenamento, uma unidade de disco está sempre disponível para o uso imediato.

O IBM 7030 possui fita magnética, leitores de cartão, perfuradores e impressoras, além de outros dispositivos extras, sendo todos compatíveis com outros computadores da IBM.

Apesar de sua alta performance, o IBM 7030 foi mais lento do que se esperava e acabou não atingindo as expectativas de seus criadores. Com isso a IBM foi forçada a baixar seu preço de $13,5 milhões para  $7,78 milhões.


## PDP-8


<img src="imagens/img_pdp_8.jpg" width="388" height="314">


O PDP-8 foi o primeiro sucesso comercial. Destinado para uso geral e produzido ainda na década de 1960, a máquina de 12 bits foi um minicomputador produzido pela Digital Equipment Corporation (DEC).

Seus primeiros sistemas eram baseados em transistores, diodos e resistores individuais. No final da década de 1960, os circuitos integrados TTL vieram para substituir esses componentes e simplificar a produção do PDP-8. E em um processo de economia e otimização do projeto, os circuitos integrados CMOS personalizados foram utilizados no final da década de 1970.

No início da década de 1970 foi onde o PDP-8 ficou mais popular, era o primeiro computador acessível ao bolso e que poderia ser comprado e não alugado. Muitas empresas investiram no PDP-8 em operações comerciais, incluindo contabilidade e controle de estoque.

PDP significa Processador de dados programável. O processador PDP-8, sua memória e seus circuitos de controle não são muito difíceis vistos por padrões atuais. Diversos emuladores de softwares conseguem emular programas PDP e grande parte de sua documentação original é facilmente encontrada na internet




