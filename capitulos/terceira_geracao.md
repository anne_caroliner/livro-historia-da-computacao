# TERCEIRA GERAÇÃO DE COMPUTADORES


1965 a 1975: nascem os computadores da terceira geração. Esses computadores passam a ter diversos componentes miniaturizados e montados em um único CHIP, sendo capazes de calcular em nanossegundos, com uma linguagem de programação de alto nível, orientada para os procedimentos.Os primeiros computadores a usarem circuitos integrados foram os modelos B3500 e o B3600 produzidos pela Burroughs Corporation e os computadores da linha IBM System/360, compatíveis em nível de software, que foram um sucesso na época, vendendo milhares de K350


<img src="imagens/img_tres.jpeg" width="410" height="314">


O IBM System/360 (S/360) constitui-se numa família de mainframes lançada pela IBM em 7 de abril de 1964. O 360 incluía um processador central e muitos periféricos, determinando várias opções de expansão. Ou seja, o 360 foi o primeiro a apresentar o conceito de modularidade: o comprador poderia adquirir diferentes módulos, conforme suas necessidades. Essa flexibilidade permitiu que várias empresas comprassem seu primeiro computador.


<img src="imagens/img_quatro.jpeg" width="410" height="314">


## DESENVOLVIMENTO DOS CIRCUITOS INTEGRADOS


Na década de 50, Geoffrey W. A. Dummer desenvolveu a ideia inicial de um Circuito integrado (CI):
um circuito eletrônico miniaturizado (composto principalmente por dispositivos semicondutores) sobre um substrato fino de material semicondutor.O uso de transistores passou a ser substituído pelo de circuitos integrados, unidades de encapsulamento semicondutoras que agrupam transistores, resistores, diodos e outros componentes elétricos interligados em uma pastilha de Silício e Germânio, que passaram a ser conhecidos como chips. As duas variedades de circuitos integrados da geração eram o SSI (Circuitos de pequena escala) com cerca de 10 transistores por chip e os MSI (Circuitos de média escala) com cerca de 100 transistores por chip.


