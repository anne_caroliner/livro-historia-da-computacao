# A quarta Geração da computação

A quarta geração (1970 - atualidade) caracteriza-se pela aprimoração das tecnologias já existentes. Isso se deu pelo advento dos microprocessadores pessoais, que fizeram com que o preço e o tamanho das máquinas reduzissem drasticamente, e a compra de um computador pessoal foi ficando cada vez mais fácil. As CPUs passaram a realizar milhões de operações por segundo, o que tornou possível a realização de diversas tarefas, o software se tornou tão importante quanto o hardware, o que colaborou para que o uso do computador se tornasse mais fácil.

### A criação da Internet

A internet que conhecemos hoje tem origem na Arpanet, rede criada entre 1969 e 1972. a Arpanet possuía apenas 4 nós (sediados em Stanford Research Institute, na Universidade da Califórnia, na Universidade de Santa Barbara e na Universidade de Utah, todos nos Estados Unidos), que eram interligados utilizando linhas telefônicas modificadas. Em 1974, foi criado o TCP/IP, que acabou se tornando protocolo para o uso na Arpanet, e mais tarde na Internet.

![Arpanet](https://www.oficinadanet.com.br/imagens/post/13707/977-8.jpg)


### Altair

Utilizando o microprocessador da Intel, o Altair 8800 era menor (tamanho suficiente para caber em uma mesa) e mais eficiente, o que o tornou revolucionário ao que se conhecia como computador. O computador funcionava com cartões de entrada e saída, sem utilizar uma interface gráfica.


![Altair 8800](https://img.ibxk.com.br/materias/altair8800.jpg?w=704)


### Apple

Apesar de o Altair 8800 ter iniciado a revolução, considera-se que o primeiro computador pessoal foi o Apple I, lançado em 1976. A decisão de aperfeiçoar o projeto veio de fato que o computador era de difícil uso e com uma parte gráfica que deixava a desejar. Então, integrou à máquina um monitor que mostrava o que acontecia no PC, obtendo grande sucesso. 
Outros modelos que sucedem o Apple I são Lisa (1983) e Macintosh (1984), que mouse e a interface que conhecemos atualmente.

![Macintosh](http://s2.glbimg.com/ms_WIGLMc_n5_mClE-7ETQ9DK68=/0x0:695x483/695x483/s.glbimg.com/po/tt2/f/original/2014/01/24/copia-de-captura-de-tela-2014-01-24-as-103005.png)

### Microsoft

Fundada paralelamente à Apple, a Microsoft utilizou-se de ideias implementadas em outros sistemas. Assim foi criado o próprio sistema operacional da Microsoft, o MS-DOS, que estava muito à frente do sistema desenvolvido por Jobs. 

![MS-DOS](https://www.estudopratico.com.br/wp-content/uploads/2015/09/sistema-operacional-ms-dos.png)

Então, Bill Gates, criador da Microsoft, criou uma parceria com Jobs, e, após um tempo, copiou toda a interface gráfica do macintosh para seu sistema operacional Windows, tornando a Microsoft uma grande concorrente da Apple. Com a eventual demissão de Steve Jobs da Apple, a Microsoft acabou tornando-se a líder no mercado de computadores.
Desde então, processadores Intel passaram a ser lançados acompanhados do Windows.

![Pocessador Intel pentium](https://img.ibxk.com.br/materias/2157/728653.jpg?w=704)

### Multicore

Um processador é a parte mais importante do computador, uma vez que realiza quase todos os cálculos, mas a parte mais essencial é seu núcleo. Assim, a fim de torná-lo mais eficiente, empresas que fabricam processadores decidiram colocar dois, ou até mais, núcleos em uma mesma peça — um processador multicore que, entre outras vantagens, pode executar mais tarefas ao mesmo tempo e evitar o superaquecimento do aparelho.

### Computação de bolso

Com a grande evolução e desenvolvimento das tecnologias, é possível cada vez mais integrar funções em um mesmo aparelho, resultando em celulares e tablets capazes de empreender quase todas as funções de um computador, possuindo sistemas operacionais completos, e ainda tendo a vantagem de serem portáteis e acessíveis.
![Celular](https://d1fmx1rbmqrxrr.cloudfront.net/cnet/i/edit/2020/03/samsung-galaxy-s20-plus-big.jpg)
