# PRIMEIRA GERAÇÃO DE COMPUTADORES


Os computadores da primeira geração foram criados baseado em uso de relés e válvulas eletrônicas para realização de cálculos e a grande vantagem dessa mudança em relação  às máquinas de calcular mecânicas era  a maior velocidade no processamento.
Devido ao seu grande consumo de energia os computadores da primeira geração sofriam muitos problemas, tendo pouca durabilidade, além de precisar ser reprogramado a cada tarefa.

## ENIAC(Eletronic Numerical Integrator and Computer)


<img src="imagens/img_eniac.jpeg" width="388" height="314">


O ENIAC foi o primeiro computador digital eletrônico e programável de uso geral, construído durante a segunda guerra mundial nos Estados Unidos. Usando plugboards para comunicar suas instruções tinha a vantagem que, uma vez configuradas as instruções, a máquina continuava funcionando em velocidade eletrônica. Sua entrada de dados era baseada em cartões perfurados mas apesar das dúvidas, o ENIAC esteve ativo por mais de 10 anos.

Um computador de grande porte, ocupava um porão de até 15 metros, o ENIAC tinha 40 painéis dispostos em forma de U, alinhado em três paredes. Cada painel tinha cerca de 0,6 metros por 0,6 metros por 2,4 metros com pouco mais de 17.000 tubos de vácuo, 70.000 resistores, 6.000 interruptores e 1.500 relés, foi um dos sistemas eletrônicos mais difíceis construídos até então.

Criado para ajudar a vencer a guerra e finalizado no ano de 1946, a guerra já havia terminado e o ENIAC tinha o valor de US $400.000 para o governo. Sua primeira tarefa foi criar cálculos para a construção de uma bomba de hidrogênio. 

## Harvard MARK I


<img src="imagens/img_mark.jpg" width="388" height="314">


Também criado durante a Segunda Guerra Mundial, chamado Harvard-IBM Automatic Sequence Controlled Calculator (Calculador controlado de sequências automáticas Harvard - IBM). Apoiado pela IBM e a Marinha dos Estados Unidos, um grupo de engenheiros da IBM liderado por Clair D. Lake criaram no ano de 1944 o MARK I, considerado o primeiro projeto de computador.

Uma calculadora eletromecânica de grande porte com integração de conceitos de computadores digitais e analógicos, media 2,5 m de altura e 18m de comprimento. Sua eletrônica era baseada em muitas válvulas e suas operações internas eram controladas por relés.

O MARK I era operado através de fita de papel onde era armazenado por meio de perfurações as instruções codificadas. Uma vez programado, o computador conseguia calcular uma sequência de operações aritmética com números compostos por até 23 dígitos de forma automática.

Entre os anos de 1947 e 1952 foram construídas versões melhoradas do MARK I, os MARK II, III e IV.
